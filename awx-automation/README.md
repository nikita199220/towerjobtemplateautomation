# AWX Template Automation

## Purpose
Create the Ansible tower templates using ansible playbook

## Prerequisites
i)	Valid Ansible tower URL
ii)	Credentials with admin privileges

## Code Description
Variables:
```
          awx_server: {{ AWX server Details }}
          username: {{ username }}
          password: {{ password }} 
          project_name: {{ project name }}
          organization_id: {{ organization id }}
          scm_type: # Possible values git or {{ blank }} 
          # Blank  when source code is on server
          # git  when source code is on source control
          scm_url: {{ source control url }} # when SCM Type git
          inventory_name: {{ inventory name  }}
          credential_name: {{ credential name }}
          credential_type: 1
          host_username: {{ host user to run the playbook }}
          host_password: {{ host password to run the playbook }}
          template_name: {{ template name }}
          playbook_name: {{ playbook name }}
          project_directory: {{ project directory }} # when SCM Type manual
```
## Steps:

##### Create SCM Project 
```
      - name: Create Git Project in AWX 
        uri:
          url: http://{{ awx_server }}/api/v2/projects/
          user: "{{ username }}"
          password: "{{ password }}"
          method: POST
          body: "{ \"allow_override\": \"true\", \"name\": \"{{ project_name }}\", \"organization\": {{ organization_id }}, \"scm_type\": \"{{ scm_type }}\", \"scm_url\": \"{{ scm_url }}\" }" 
          force_basic_auth: yes
          body_format: json
          status_code: 200, 201, 400
          return_content:  yes
        register: project_details
        when: scm_type == "git"
```

##### Create manual Project in Ansible Tower or AWX
```
      - name: Create Manual Project in AWX 
        uri:
          url: http://{{ awx_server }}/api/v2/projects/
          user: "{{ username }}"
          password: "{{ password }}"
          method: POST
          body: "{ \"name\": \"{{ project_name }}\", \"description\": \"\", \"local_path\": \"{{ project_directory }}\", \"scm_type\": \"\", \"scm_url\": \"\", \"scm_branch\": \"\" }"
          force_basic_auth: yes
          body_format: json
          status_code: 200, 201, 400
          return_content:  yes
        register: project_details
        when: scm_type != "git"
```

##### Create Inventory
```

      - name: Create Inventory
        uri:
          url: http://{{ awx_server }}/api/v2/inventories/
          user: "{{ username }}"
          password: "{{ password }}"
          method: POST
          body: "{ \"name\": \"{{ inventory_name }}\", \"organization\": {{ organization_id }} }"
          force_basic_auth: yes
          body_format: json
          status_code: 200, 201, 400
```
##### Create Credential
```

      - name: Create host Credential
        uri:
          url: http://{{ awx_server }}/api/v2/credentials/
          user: "{{ username }}"
          password: "{{ password }}"
          method: POST
          body: "{ \"name\": \"{{ credential_name }}\", \"description\": \" \", \"organization\": {{ organization_id }}, \"credential_type\": {{ credential_type }}, \"inputs\": { \"password\": \"{{ host_password }}\", \"username\": \"{{ host_username }}\"} }"
          force_basic_auth: yes
          body_format: json
          status_code: 200, 201, 400
```

##### Create Job template
```

      - name: Create Job Templates
        uri:
          url: http://{{ awx_server }}/api/v2/job_templates/
          user: "{{ username }}"
          password: "{{ password }}"
          method: POST
          body: "{ \"name\": \"{{ template_name }}\", \"description\": \"Created via playbook\", \"job_type\": \"run\", \"inventory\": \"{{ fetch_inventory_id }}\", \"project\": \"{{ fetch_project_id }}\", \"playbook\": \"{{ playbook_name }}\", \"verbosity\": 0 }"
          force_basic_auth: yes
          body_format: json
          status_code: 200, 201, 400
```

##### Create Job Templates with extra vars
``` 
          # - name: Create Job Templates
          #   uri:
          #     url: http://{{ awx_server }}/api/v2/job_templates/
          #     user: "{{ username }}"
          #     password: "{{ password }}"
          #     method: POST
          #     body: "{ \"name\": \"{{ template_name }}\", \"description\": \"Created via playbook\", \"job_type\": \"run\", \"inventory\": \"{{ fetch_inventory_id }}\", \"project\": \"{{ fetch_project_id }}\", \"playbook\": \"{{ playbook_name }}\", \"verbosity\": 0 \"extra_vars\": \"{{ variable1 }}: {{ value1 }}\n{{ variable2 }}: {{ value2 }}\" }"
          #     force_basic_auth: yes
          #     body_format: json
          #     status_code: 200, 201, 400
```

##### Add credential in job template 
```
        - name: Update the credentials in the job template
          uri:
            url: http://{{ awx_server }}/api/v2/job_templates/{{ job_template_id }}/credentials/
            user: "{{ username }}"
            password: "{{ password }}"
            method: POST
            body: "{ \"id\": {{ credential_id }} }"
            force_basic_auth: yes
            body_format: json
            status_code: 200, 201, 400, 204
```

## Steps to run 
```
        ansible-playbook {{playbook name}} -vv
```

To run the ansible tower template you just need to add host in inventory.
Adding hosts in not the part of this situation because static inventory is not required by any customer. Dynamic Inventory is considered here.
 

## Conclusion

Playbook will help the users to Create Ansible tower templates on the fly.

Code for reference:
<a href="https://gitlab.com/nikita199220/towerjobtemplateautomation.git
" target="_blank"><img src="https://gitlab.com/nikita199220/towerjobtemplateautomation.git" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>

License
-------

BSD

Author Information
------------------
Nikita Sharma
Devops Engineer
